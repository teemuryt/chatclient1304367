package com.example.teemu.myfirstapp;

import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.io.InputStreamReader;
import android.os.Handler;

/**
 * Created by Teemu on 19.9.2015.
 */
public class MessageReceiver implements Runnable {
InetSocketAddress ip;
    BufferedReader br;
    Socket rs; //receiver socket
    Handler handler;
    public MessageReceiver(Socket b, InetSocketAddress isa, Handler h){
        this.handler = h;
        this.rs = b;
        this.ip = isa;

    }

    @Override
    public void run() {
        try {
            rs.connect(ip);
            Log.e("Connected", "Connected");
            br = new BufferedReader(new InputStreamReader(rs.getInputStream()));
            while(true){
                String viesti = br.readLine();
                while (viesti!=null){
                    Message msg = handler.obtainMessage();
                    msg.obj = viesti;
                    msg.what = 0;
                    if(!viesti.equals("")) {
                        handler.sendMessage(msg);
                    }
                    viesti = br.readLine();
                }
                br.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Catch", "Catch");
        }
    }
}
