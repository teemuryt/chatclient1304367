package com.example.teemu.myfirstapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class MyActivity extends AppCompatActivity {

    private Handler uiHandler = new
                    Handler()
                    {
                        public
                        void
                        handleMessage(Message
                                              msg)
                        {
                            if(msg.what==0){
                                TextView result = (TextView)findViewById(R.id.laatikko);
                                result.append((String)msg.obj + "\n");
                            }
                        }
                    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        final TextView ikkuna = (TextView)findViewById(R.id.laatikko);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        final Socket bb = new Socket();
        InetSocketAddress ip = new InetSocketAddress("10.112.214.127",100);

        MessageReceiver mr = new MessageReceiver(bb,ip,uiHandler);
        Thread t1 = new Thread(mr);
        t1.start(); //starting connection
        Button btn = (Button)findViewById(R.id.SendButton);
        final EditText edit = (EditText)findViewById(R.id.TextReceiver);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String text = edit.getText().toString();
                ikkuna.append("\n" + text);
                MessageSender mS = new MessageSender(text,bb);
                Thread t2 = new Thread(mS);
                t2.start(); //starting text feed with thread


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
